# **Oblig 2 INF226**

## Whats wrong with the structure and does it pose a securtiy risk?

As already mentioned in the task itself, the code is badly structured as it has everything inside one file. 
This makes it much harder to debug as you have to search through much more code. Additionally, code that are otherwise completely unrelated, could end up casuing problems for eachother, and thus making debugging even more tedious. And as we know good, and easily maintainable code is often secure as well. This goes the opposite direction as well, messy, unorganized code is often riddled with bugs aswell. Bugs and bad organization is recipie for security risks.

The usernames and passwords are stored in plain text, which is an uncceptable security risk. Not that it would really matter that much though as the login system doesn't even check for a matching password, and your username is not really a secret. There is also no protection whatsoever against SQL-injections and XSS attacks. '); DROP TABLE messages; will remove all messages and prevent any new messages from being created and sent, effectively destroying the entire application.
There is also nothing that check who sent what, in other words everyone can write in eachothers name. All you need to do is change the name in "From" to whoever you wish to impersonate. This application is also highly vulnerable to CSRF-attacks, as you can litterally do changes and send messages from other users using the URL bar. http://localhost:5000/send?sender=Bob&message=Hi will send the message "Hi" from Bob, even if it is not Bob who wrote this in the URL. You can even click the URL and the message command within the URL will run, thus making it not only possible, but very easy to use this in a potential phishing attack.


## Features and security concerns
I added a register form that would have let you create a new user and the password for that user, if it had been finished. I made it accessable from the login page by creating a new button that links to the register page, it is also possible to return from the register page and back to the login page. I created a logout button on the index page, you can find it in the bottom left corner. For the register functionality I also created some passowrd restrictions, which are: Atleast one each of the following: capital letter, lowercase letter, digit and non-alphanumeric symbol. I was going to add restrictions to prevent dangerous symbols in the username and password, by dangerous symbols I mean symbols that can potentially be used to perform a SQL injection. Even better would have been to sanitize the information before it gets sent further to the sensitive parts of the program, this way there would be no need for blacklists, and it would likely been even more secure (blacklist aren't the most effective).

## Threat model, attack vectors and solutions
Currently, with the abysmal security of this program, everyone with a computer could easily become dangerous adversaries. However, the most likely adversary would be script-kiddies as there really isn't anything valuable to be stolen/destroyed. However if we think of chatting platforms at large, then there would be a lot of possible adversaries. The most common threat would probably be people who tries to breach the database in order to access usernames and passwords. DDOS attacks is also a fairly common attack at popular sites (again we are now talking about chat programs in general, not just this localhost program). 
How do we avoid these attacks? For the server breach/password-leak attacks we should make sure we don't save passwords in plain text (which I planned to fix, but did not have time) and take measures to prevent SQL-injections. When it comes to preventing DDOS attacks we could use programs that scans for and recognize suspicious traffic and blocks it. A DDOS attack will harm the programs availability, while a server breach is harming the confidentiality of the secret information. SQL injections have the potential to harm the programs integrity. The unfortunate truth is that the program is very susceptible to all of these attacks.

The main attack vectors against this program would probably be SQL-injections, XSS attacks and CSRF attacks.

Like already mentioned, to avoid SQL injections we should sanitize the input before it get processed by the the sensitve parts. The program also has no way of keeping track of who is doing what as I didn't have time to implement proper user IDs and every user can change the username however and whenever they like. There is also no real tracability, meaning that an attack would probably go undetected for a long time. 

To improve security, I should have stored the usernames and password in a database instead of storing it directly in the code. Again, due to a lack of time, I did not manage to implement that.

The program doesn't really have a access control system. In the future it would probably be best to implement a ReBAC style of access control.

Like said above, there really is no tracability and logging in this program, making it very hard to track attacks. Knowing when the security is "good enough" is nearly impossibe and you will always have to balance usability and security. If you have to rigid security, you risk causing unnessesary irritability among the users. Example: No one would use a chatting program that required you to login and solve a CAPTCHA every time you would send a message. And yes, there are limits to what we can sensibly protect against.

## How to test the program
Make sure you have installed flask and flask_wtf. Start the program by using **flask run**. You will now find the program at **http://localhost:5000** and it will automatically redirect you to the login page.